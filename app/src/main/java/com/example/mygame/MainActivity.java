package com.example.mygame;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    ArrayList<word> listWord = new ArrayList<>();
    ArrayList<Boolean> added = new ArrayList<>();
    ArrayList<Question> questions = new ArrayList<>();
    int currentquestion = 0;
    Question temtquestion;
    int totalQuestion;
    int score = 0;
    TextView ans1, ans2, ans3, ans4;
    TextView orderquestion, contentquestion, tvscore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listWord.add(new word("team", "nhóm"));
        listWord.add(new word("meet", "họp"));
        listWord.add(new word("miss", "nhơ"));
        listWord.add(new word("six", "sáu"));
        listWord.add(new word("ten", "mười"));
        added.add(false);
        added.add(false);
        added.add(false);
        added.add(false);
        added.add(false);
        totalQuestion = listWord.size();
        Collections.shuffle(listWord);
        intiuni();
        getQuestion();
        if (questions.isEmpty()) return;
        loadQuestion(questions.get(currentquestion));



    }

    private void loadQuestion(Question question) {
        if(question == null) return;
        temtquestion = question;
        String numberquestion = "Question " + (currentquestion  + 1) + ":";
        orderquestion.setText(numberquestion);
        tvscore.setText("Điểm số: 0/" + totalQuestion);
        ans1.setBackgroundResource(R.drawable.blue_corner);
        ans2.setBackgroundResource(R.drawable.blue_corner);
        ans3.setBackgroundResource(R.drawable.blue_corner);
        ans4.setBackgroundResource(R.drawable.blue_corner);

        contentquestion.setText(listWord.get(currentquestion).getEngWord());
        ans1.setText(questions.get(currentquestion).getAnswer().get(0));
        ans2.setText(questions.get(currentquestion).getAnswer().get(1));
        ans3.setText(questions.get(currentquestion).getAnswer().get(2));
        ans4.setText(questions.get(currentquestion).getAnswer().get(3));

        ans1.setOnClickListener(this);
        ans2.setOnClickListener(this);
        ans3.setOnClickListener(this);
        ans4.setOnClickListener(this);

    }

    private ArrayList<Question> getQuestion() {
        for (int i = 0; i < totalQuestion; i++) {
            Question qu = creatQuestion(listWord.get(i), i);
            questions.add(qu);
        }
        return questions;
    }

    private Question creatQuestion(word word,int j) {
        Question question;
        ArrayList<String>listans = new ArrayList<>();
        listans.add(word.getVieWord());
        Random random = new Random();
        int randomPos = random.nextInt(totalQuestion);
        Collections.fill(added, Boolean.FALSE);
        added.set(j, true);
        for(int i = 0; i < 3; i++){
            while (randomPos == totalQuestion || added.get(randomPos))
                randomPos = random.nextInt(listWord.size());
            listans.add(listWord.get(randomPos).getVieWord());
            added.set(randomPos, true);
        }
        Collections.shuffle(listans);
        question = new Question(listans, word);
        return question;
    }

    private void intiuni() {
        ans1 = findViewById(R.id.tvans1);
        ans2 = findViewById(R.id.tvans2);
        ans3 = findViewById(R.id.tvans3);
        ans4 = findViewById(R.id.tvans4);
        tvscore = findViewById(R.id.tvscore);
        orderquestion = findViewById(R.id.tvorder);
        contentquestion = findViewById(R.id.tvquestion);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvans1:
                ans1.setBackgroundResource(R.drawable.yellow_coner);
                checkAnswer(ans1, temtquestion, temtquestion.getAnswer().get(0));
                break;
            case R.id.tvans2:
                ans2.setBackgroundResource(R.drawable.yellow_coner);
                checkAnswer(ans2, temtquestion, temtquestion.getAnswer().get(1));
                break;
            case R.id.tvans3:
                ans3.setBackgroundResource(R.drawable.yellow_coner);
                checkAnswer(ans3, temtquestion, temtquestion.getAnswer().get(2));
                break;
            case R.id.tvans4:
                ans4.setBackgroundResource(R.drawable.yellow_coner);
                checkAnswer(ans4, temtquestion, temtquestion.getAnswer().get(3));
                break;
        }
    }

    private void checkAnswer(TextView textView, Question question, String answer) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(answer.equals(question.getContent().getVieWord())) {
                    textView.setBackgroundResource(R.drawable.green_coner);
                    score ++;
                    tvscore.setText("Điểm số: " + score + "/" + totalQuestion);
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.correct);
                    mp.start();
                } else {
                    textView.setBackgroundResource(R.drawable.red_coner);
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.incorrect);
                    mp.start();
                    showAnswerCorrect(temtquestion);

                }
                nextQuestion();
            }
        }, 1000);
    }

    private void showAnswerCorrect(Question question) {
        if (question == null || question.getAnswer() == null) {
            return;
        }
        int i = question.findCorrect();
        if (i == 0) {
            ans1.setBackgroundResource(R.drawable.green_coner);
        } else if(i == 1) {
            ans2.setBackgroundResource(R.drawable.green_coner);
        } else if (i == 2) {
            ans3.setBackgroundResource(R.drawable.green_coner);
        } else if (i == 3) {
            ans4.setBackgroundResource(R.drawable.green_coner);
        }
    }

    private void nextQuestion() {
        if (currentquestion == totalQuestion - 1) {
            showDialog("end game");
        } else {
            currentquestion++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadQuestion(questions.get(currentquestion));
                }
            }, 2000);

        }
    }
    private void  showDialog(String mess){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mess).setCancelable(false).setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentquestion = 0;
                score = 0;
                loadQuestion(questions.get(currentquestion));
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}