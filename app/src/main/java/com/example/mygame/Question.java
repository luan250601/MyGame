package com.example.mygame;

import java.util.ArrayList;

public class Question {
    private ArrayList<String> Answer;
    private word content;

    public Question(ArrayList<String> answer, word content) {
        Answer = answer;
        this.content = content;
    }

    public ArrayList<String> getAnswer() {
        return Answer;
    }

    public void setAnswer(ArrayList<String> answer) {
        Answer = answer;
    }

    public word getContent() {
        return content;
    }

    public void setContent(word content) {
        this.content = content;
    }
    public int findCorrect() {
        for (int i = 0; i < 4; i++) {
            if((content.getVieWord()).equals(Answer.get(i))) {
                return i;
            }
        }
        return 0;
    }
}
