package com.example.mygame;

public class word {
    private String EngWord;
    private String VieWord;

    public word(String engWord, String vieWord) {
        EngWord = engWord;
        VieWord = vieWord;
    }

    public String getEngWord() {
        return EngWord;
    }

    public void setEngWord(String engWord) {
        EngWord = engWord;
    }

    public String getVieWord() {
        return VieWord;
    }

    public void setVieWord(String vieWord) {
        VieWord = vieWord;
    }
}
